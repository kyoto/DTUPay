import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/*
 @Author = Kacper
 */
@JsonPropertyOrder({"CPR" , "username"})
public class Customer {
	private String username;
	private String cpr;

	public Customer(String username, String cpr) {
		this.username = username;
		this.cpr = cpr;
	}
	
	public Customer() {super();}

	public String getCustomerUsername() {
		return username;
	}

	public String getCustomerCPR() {
		return cpr;
	}

	public void setCustomerUsername(String username) {
		this.username = username;
	}
	
	public void setCustomerCPR(String cpr) {
		this.cpr = cpr;
	}

}
