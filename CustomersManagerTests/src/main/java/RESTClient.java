import gherkin.deps.com.google.gson.Gson;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/*
 @Author = Kacper
 */
public class RESTClient {
    Client client = ClientBuilder.newClient();
    Gson gson = new Gson();


    public Response createCustomer(String CPR, String username){
        WebTarget target = client.target("http://localhost:8995").path("/customers");
        Customer customer = new Customer(username, CPR);
        String json = gson.toJson(customer);
        Response response = target
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.entity(json, MediaType.APPLICATION_JSON));
        return response;
    }

    public Response deleteCustomer(String CPR){
        WebTarget target = client.target("http://localhost:8995").path("/customers");
        Response response = target.path(CPR)
                .request(MediaType.TEXT_PLAIN)
                .delete();
        return response;
    }

    public Response generateTokens(String CPR, int count){
        WebTarget target = client.target("http://localhost:8995").path("/customers/tokens");
        Response response = target.queryParam("count", count)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(CPR, MediaType.TEXT_PLAIN));
        return response;
    }

}

