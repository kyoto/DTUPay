import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;

/*
 @Author = Gudmundur
 */
public class RESTTest {
    RESTClient client = new RESTClient();


    //Create and delete Customer
    @Test
    public void manageCustomer(){
        String CPR = "123456";
        String username = "testName";
        Response response = client.createCustomer(CPR, username);
        Assert.assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
        response = client.deleteCustomer(CPR);
        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }


}
