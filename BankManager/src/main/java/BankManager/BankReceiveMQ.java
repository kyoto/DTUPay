package BankManager;


import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;


import com.rabbitmq.client.*;
import gherkin.deps.com.google.gson.Gson;

/*
 @Author = Jacob
 */
public class BankReceiveMQ {

    private static final String RPC_QUEUE_NAME = "Tokens";
    private static BankManager bank = new BankManager();
    private static Gson gson = new Gson();

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
//        factory.setHost("localhost");

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null);
            channel.queuePurge(RPC_QUEUE_NAME);

            channel.basicQos(1);

            System.out.println("Waiting for a message...");

            Object monitor = new Object();
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                        .Builder()
                        .correlationId(delivery.getProperties().getCorrelationId())
                        .build();


                // THIS IS THE RESPONSE STRING
                String status = "";
                Message response = new Message();
                try {
                    // THIS IS THE STRING YOU GOT
                    String message = new String(delivery.getBody(), "UTF-8");
                    Message paymentMessage = gson.fromJson(message, Message.class);
                    PaymentRequest paymentRequest = gson.fromJson(paymentMessage.getPayload(), PaymentRequest.class);

//                    JSONObject jo = new JSONObject(message);
//                    String msg = jo.getString("msg");
//                    BigDecimal price = BigDecimal.valueOf(jo.getInt("price"));
//                    String cpr = jo.getString("cpr");
//                    String cvr = jo.getString("cvr");

                    // MAKE A PAYMENT
                    boolean noException = bank.transferMoney(paymentRequest);



                    System.out.println("Message: " + paymentMessage.getPayload());

                    // HERE YOU BUILD A RESPONSE
                    if(noException) {
                        status += "SUCCESS";
                        response.setStatus("OK");
                    }
                    else{
                        status += "FAIL";
                        response.setStatus("ERROR");
                    }
                    response.setPayload(status);

                } catch (RuntimeException e) {
                    System.out.println(" [.] " + e.toString());
                    status += "FAIL";
                    response.setStatus("ERROR");
                    response.setPayload(status);

                } finally {


                    String json = gson.toJson(response);
                    // HERE IT'S SENT BACK
                    channel.basicPublish("", delivery.getProperties().getReplyTo(), replyProps, json.getBytes("UTF-8"));
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                    // RabbitMq consumer worker thread notifies the RPC server owner thread
                    synchronized (monitor) {
                        monitor.notify();
                    }
                }
            };

            channel.basicConsume(RPC_QUEUE_NAME, false, deliverCallback, (consumerTag -> { }));
            // Wait and be prepared to consume the message from RPC client.
            while (true) {
                synchronized (monitor) {
                    try {
                        monitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
