package BankManager;


import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;


import java.math.BigDecimal;

/*
 @Author = Gudmundur
 */
// class wrapping bankService
public class BankManager {

    private BankService bankService;

    public BankManager() {
        BankServiceService bankServiceService = new BankServiceService();
        bankService = bankServiceService.getBankServicePort();
    }

    public BigDecimal getUserBalance(String accountNumber){
        try {
            return bankService.getAccount(accountNumber).getBalance();
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean transferMoney(PaymentRequest paymentRequest) {
        try {
            String creditorAccount = paymentRequest.getCreditorAccountNumber();
            String debtorAccount = paymentRequest.getDebtorAccountNumber();
            bankService.transferMoneyFromTo(debtorAccount, creditorAccount,
                    new BigDecimal(paymentRequest.getPrice()), paymentRequest.getPaymentMessage());
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
