//import com.rabbitmq.client.AMQP;
//import com.rabbitmq.client.Channel;
//import com.rabbitmq.client.Connection;
//import com.rabbitmq.client.ConnectionFactory;
//import org.json.JSONObject;
//
//import java.io.IOException;
//import java.util.UUID;
//import java.util.concurrent.ArrayBlockingQueue;
//import java.util.concurrent.BlockingQueue;
//import java.util.concurrent.TimeoutException;
//
//public class TestMSender implements AutoCloseable {
//
//    private Connection connection;
//    private Channel channel;
//    private String requestQueueName = "payment_rpc_queue";
//
//    private TestMSender() throws IOException, TimeoutException {
//        ConnectionFactory factory = new ConnectionFactory();
//        factory.setHost("localhost");
//
//        connection = factory.newConnection();
//        channel = connection.createChannel();
//    }
//
//    public static void main(String[] argv) {
//        try (TestMSender paymentRpc = new TestMSender()) {
//
//            // THIS IS YOUR MESSAGE
//            JSONObject jo = new JSONObject();
//            jo.put("cvr", "123");
//            jo.put("cpr", "456");
//            jo.put("price", 100);
//            jo.put("msg", "payment");
//
//
//            String message = jo.toString();
//
//            // HERE YOU REQUEST AND GET RESPONSE
//            String response = paymentRpc.call(message);
//            System.out.println("Response: " + response);
//
//        } catch (IOException | TimeoutException | InterruptedException e) {
//            e.printStackTrace();
//        }
////        return true;
//    }
//
//    private String call(String message) throws IOException, InterruptedException {
//        final String corrId = UUID.randomUUID().toString();
//
//        String replyQueueName = channel.queueDeclare().getQueue();
//        AMQP.BasicProperties props = new AMQP.BasicProperties
//                .Builder()
//                .correlationId(corrId)
//                .replyTo(replyQueueName)
//                .build();
//
//        channel.basicPublish("", requestQueueName, props, message.getBytes("UTF-8"));
//
//        final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);
//
//        String ctag = channel.basicConsume(replyQueueName, true, (consumerTag, delivery) -> {
//            if (delivery.getProperties().getCorrelationId().equals(corrId)) {
//                response.offer(new String(delivery.getBody(), "UTF-8"));
//            }
//        }, consumerTag -> {
//        });
//
//        String result = response.take();
//        channel.basicCancel(ctag);
//        return result;
//    }
//
//    public void close() throws IOException {
//        connection.close();
//    }
//}
