import BankManager.BankManager;
import BankManager.PaymentRequest;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

/*
 @Author = Thomas
 */
public class BankManagerTest {

    private String debtorAccountNumber;
    private String creditorAccountNumber;
    private User debtor;
    private User creditor;
    private BankService bankService;

    @Before
    public void setup(){
        BankServiceService bankServiceService = new BankServiceService();
        bankService = bankServiceService.getBankServicePort();

        debtor = new User();
        creditor = new User();

        String cpr = "11111111";
        String debtor_firstname = "Debtor";
        String debtor_lastname = "Debtorsky";

        debtor.setCprNumber(cpr);
        debtor.setFirstName(debtor_firstname);
        debtor.setLastName(debtor_lastname);

        String cvr = "22222222";
        String credior_firstname = "Creditor";
        String creditor_lastname = "Creditorsky";

        creditor.setCprNumber(cvr);
        creditor.setFirstName(credior_firstname);
        creditor.setLastName(creditor_lastname);

        try {
            bankService.createAccountWithBalance(debtor, new BigDecimal(100));
            bankService.createAccountWithBalance(creditor, new BigDecimal(100));


        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
        try {
            creditorAccountNumber = bankService.getAccountByCprNumber(creditor.getCprNumber()).getId();
            debtorAccountNumber = bankService.getAccountByCprNumber(debtor.getCprNumber()).getId();
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void getAccount() {
        Assert.assertNotNull(creditorAccountNumber);
        Assert.assertNotNull(debtorAccountNumber);
    }

    @Test
    public void userBalance() {
        BankManager bm = new BankManager();
        Assert.assertEquals(new BigDecimal(100), bm.getUserBalance(debtorAccountNumber));
    }

    @Test
    public void transferMoney(){

        BankManager bm = new BankManager();
        bm.transferMoney(new PaymentRequest(creditorAccountNumber, debtorAccountNumber, 100, "Initial transaction"));
        Assert.assertEquals(new BigDecimal(0), bm.getUserBalance(debtorAccountNumber));
        Assert.assertEquals(new BigDecimal(200), bm.getUserBalance(creditorAccountNumber));

    }

    @After
    public void TearDown(){
        try {
            bankService.retireAccount(bankService.getAccount(debtorAccountNumber).getId());
            bankService.retireAccount(bankService.getAccount(creditorAccountNumber).getId());
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }
}
