import objects.Customer;
import database.InMemoryCustomersDatabase;
import database.ICustomerDatabase;
import org.junit.Assert;
import org.junit.Test;


/*
 @Author = Jacob
 */
public class CustomersManagerTest {
    private ICustomerDatabase database = new InMemoryCustomersDatabase();

    @Test
    public void addingCustomers() {
        Customer customer = new Customer("123", "456", "Customersky");
        database.insertCustomer(customer);
        Assert.assertTrue(database.containsCustomer(customer));
    }


    @Test
    public void deletingCustomers() {
        Customer customer = new Customer("222", "333", "Custorsky");
        database.insertCustomer(customer);
        Assert.assertTrue(database.containsCustomer(customer));
        database.deleteCustomer(customer.getCpr());
        Assert.assertFalse(database.containsCustomer(customer));
    }


}
