package mq;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import gherkin.deps.com.google.gson.Gson;
import objects.GenerateTokensRequest;
import objects.Message;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;

/*
 @Author = Thomas
 */
public class TokensManagerRabbitMqCommunicator implements ITokensManagerMQCommunicator {

    private final static String QUEUE_NAME = "Tokens";
    private static String routingKey = "generate";
    private Connection connection;
    private Channel channel;
    private ConnectionFactory factory = new ConnectionFactory();
    private Gson gson = new Gson();

    public String requestTokens(String cpr,String accountId ,int count) throws IOException, TimeoutException, InterruptedException {
        factory.setHost("rabbitmq");
//        factory.setHost("localhost");

        connection = factory.newConnection();
        channel = connection.createChannel();
        final String corrId = UUID.randomUUID().toString();

        String replyQueueName = channel.queueDeclare().getQueue();
        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();

        GenerateTokensRequest generateTokensRequest = new GenerateTokensRequest(cpr, accountId, count);
        String payload = gson.toJson(generateTokensRequest);
//        JSONObject jo = new JSONObject().put("cprNumber", cpr).put("amount", count);
        Message message = new Message("OK", payload);
        String json = gson.toJson(message);

        channel.exchangeDeclare(QUEUE_NAME, "topic");
        channel.basicPublish(QUEUE_NAME, routingKey, props, json.getBytes("UTF-8"));

        final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);

        String ctag = channel.basicConsume(replyQueueName, true, (consumerTag, delivery) -> {
            if (delivery.getProperties().getCorrelationId().equals(corrId)) {
                response.offer(new String(delivery.getBody(), "UTF-8"));
            }
        }, consumerTag -> {
        });

        String result = response.take();
        Message resultMessage = gson.fromJson(result, Message.class);
        if(!resultMessage.getStatus().equals("OK"))
            throw new RuntimeException(resultMessage.getPayload());
        channel.basicCancel(ctag);
        connection.close();
        return resultMessage.getPayload();
    }
}
