package mq;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/*
 @Author = Thomas
 */
public interface ITokensManagerMQCommunicator {
    String requestTokens(String cpr, String accountId, int count) throws IOException, TimeoutException, InterruptedException;
}
