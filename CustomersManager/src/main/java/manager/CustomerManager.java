package manager;

import database.ICustomerDatabase;
import database.InMemoryCustomersDatabase;
import mq.ITokensManagerMQCommunicator;
import mq.TokensManagerRabbitMqCommunicator;
import objects.Customer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/*
 @Author = Thomas
 */
public class CustomerManager {

    private static ICustomerDatabase customerDB = new InMemoryCustomersDatabase();
    private static ITokensManagerMQCommunicator communicator = new TokensManagerRabbitMqCommunicator();

    private boolean customerExists(Customer customer) {
        return customerDB.containsCustomer(customer);
    }
    public boolean addCustomer(Customer customer) {
        if(customerExists(customer)) {
            System.out.println("Customer didnt exist");
            return false;
        }
        customerDB.insertCustomer(customer);
        return true;
    }
    public boolean deleteCustomer(String cpr) {
        return customerDB.deleteCustomer(cpr);
    }
    public String requestTokens(String cpr, int count) throws InterruptedException, TimeoutException, IOException {
        Customer c = customerDB.getCustomer(cpr);
        System.out.println("Customer: " + c.getCpr());
        return communicator.requestTokens(cpr, c.getAccountNumber(), count);
    }
}
