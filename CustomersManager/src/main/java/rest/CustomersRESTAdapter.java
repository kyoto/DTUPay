package rest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import gherkin.deps.com.google.gson.Gson;

import manager.CustomerManager;
import objects.Customer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/*
 @Author = Thomas
 */
@Path("/customers")
public class CustomersRESTAdapter {

    private static CustomerManager customerManager = new CustomerManager();

    @GET
    @Produces("text/plain")
    public String  doGet(){return "Customers Microservice"; }

	@POST
    @Path("/tokens")
	@Produces("application/json")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response generateTokens(String cpr, @QueryParam("count") int count) {

        String tokensJsonString;
        try {
            tokensJsonString = customerManager.requestTokens(cpr, count);
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.FORBIDDEN).entity(e.getMessage()).build();
        }

	    return Response.status(Response.Status.OK).entity(tokensJsonString).build();
	}

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response createCustomer(String data) {

	    Gson gson = new Gson();
        Customer customer = gson.fromJson(data, Customer.class);

        if (customerManager.addCustomer(customer)) {
            return Response.status(Response.Status.CREATED).entity("Successfully created").build();
        }
        return Response.status(Response.Status.FORBIDDEN).entity("Customer already exists").build();
    }

    @Path("/{CPR}")
    @DELETE
    public Response deleteCustomer(@PathParam("CPR") String cpr) {
        if (customerManager.deleteCustomer(cpr)) {
            return Response.status(Response.Status.OK).entity("Successfully deleted the account").build();
        }
        return Response.status(Response.Status.NOT_FOUND).entity("The account does not exist").build();
    }

}
