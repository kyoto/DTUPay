package rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/*
 @Author = Jacob
 */
@ApplicationPath("/")
public class RestApplication extends Application {

}
