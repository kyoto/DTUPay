package objects;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/*
 @Author = Amal
 */
@JsonPropertyOrder({"CPR" , "username"})
public class Customer {

	private String username;
	private String cpr;
	private String accountNumber;

	public Customer(String cpr, String accountNumber, String username) {
		this.username = username;
		this.cpr = cpr;
		this.accountNumber = accountNumber;
	}

	public Customer() {super();}

	public String getUsername() {
		return username;
	}

	public String getCpr() { return cpr; }

	public String getAccountNumber() { return accountNumber; }

	public void setCustomerUsername(String username) {
		this.username = username;
	}

	public void setCustomerCPR(String cpr) {
		this.cpr = cpr;
	}

	public void setAccountNumber(String accountNumber) { this.accountNumber = accountNumber; }
}
