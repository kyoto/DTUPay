package database;

import objects.Customer;

import java.util.ArrayList;

/*
 @Author = Kacper
 */
public class InMemoryCustomersDatabase implements ICustomerDatabase {

	private static ArrayList<Customer> customersDB = new ArrayList<>();

	@Override
	public void insertCustomer(Customer customer) { customersDB.add(customer); }

	@Override
	public boolean deleteCustomer(String cpr) {
		for (Customer c : customersDB) {
			if(c.getCpr().equals(cpr)) {
				customersDB.remove(c);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean containsCustomer(Customer customer) { return customersDB.contains(customer); }

	@Override
	public Customer getCustomer(String cpr) {
		for (Customer c: customersDB) {
			System.out.println("Customer in DB: " + c.getCpr());
			if(c.getCpr().equals(cpr))
				return c;
		}
		return null;
	}


}
