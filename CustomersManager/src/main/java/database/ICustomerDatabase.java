package database;

import objects.Customer;

/*
 @Author = Jacob
 */
public interface ICustomerDatabase {
	
	void insertCustomer(Customer customer);
	boolean deleteCustomer(String cpr);
	boolean containsCustomer(Customer customer);
	Customer getCustomer(String cpr);
}
