import database.DatabaseInterface;
import database.InMemoryRepository;
import generators.TokenGenerator;
import manager.TokenManager;
import objects.Token;
import objects.TokenRepresentation;
import org.junit.Assert;
import org.junit.Test;

/*
 @Author = Amal
 */
public class TokenManagerTest {

    private TokenManager tm = new TokenManager();
    private DatabaseInterface idb = new InMemoryRepository();
    private TokenRepresentation tRep = new TokenRepresentation();
    private Token token;
    private TokenGenerator tg = new TokenGenerator();


    @Test
    public void insertToken(){
        token = tg.generateToken("TestMan", "4321");
        idb.insertToken(token);
        Assert.assertEquals(1, idb.getAvailableTokens("TestMan").size());

    }

    @Test
    public void useToken(){
        token = tg.generateToken("TestMan", "4321");
        idb.insertToken(token);
        idb.updateTokenUsed(token.getTokenValue());
        Assert.assertTrue(token.isUsed());
    }

    @Test
    public void tokenExists(){
        token = tg.generateToken("TestMan", "4321");
        idb.insertToken(token);
        Assert.assertEquals(token, idb.getToken(token.getTokenValue()));
    }

}
