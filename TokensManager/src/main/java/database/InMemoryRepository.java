package database;

import cucumber.deps.com.thoughtworks.xstream.mapper.Mapper;
import objects.Token;

import java.util.ArrayList;

/*
 @Author = Jacob
 */
public class InMemoryRepository implements DatabaseInterface {
    // the local database
    private ArrayList<Token> tokenDataBase = new ArrayList<>();


    public void insertToken(Token token) {
        tokenDataBase.add(token);
    }

    public void updateTokenUsed(String token) {
        for (Token t : tokenDataBase) {
            if (t.getTokenValue().equals(token)) {
                t.setUsed(true);
            }
        }
    }

    public Token getToken(String token) throws NullPointerException {

        for (Token t : tokenDataBase) {
              if (t.getTokenValue().equals(token))
                    return t;
        }
        System.out.println("Throwing exception");
        throw new NullPointerException();
    }

    public ArrayList<Token> getAvailableTokens(String userId) {
        ArrayList<Token> tokens = new ArrayList<>();
        for (Token t : tokenDataBase) {
            if(t.getUserId().equals(userId) && !t.isUsed())
                tokens.add(t);
        }
        return tokens;
    }

    @Override
    public String getUserAccountByToken(String tokenValue) throws NullPointerException {
        for (Token t : tokenDataBase) {
            if (t.getTokenValue().equals(tokenValue)) {
                return t.getAccountId();
            }
        }
        throw new NullPointerException();
    }

}
