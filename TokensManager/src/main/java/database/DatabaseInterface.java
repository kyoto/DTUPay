package database;

import objects.Token;

import java.util.ArrayList;

/*
 @Author = Kacper
 */
public interface DatabaseInterface {

    void insertToken(Token token);

    void updateTokenUsed(String token);

    Token getToken(String token);

    ArrayList<Token> getAvailableTokens(String userId);

    String getUserAccountByToken(String tokenValue);
}
