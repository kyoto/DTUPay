package manager;

import java.util.ArrayList;
import java.util.List;

import cucumber.deps.com.thoughtworks.xstream.mapper.Mapper;
import generators.*;
import database.*;
import objects.*;

/*
 @Author = Thomas
 */
public class TokenManager {

    // object for generating tokens (objects.Token: accountNumber, string, flag)
    private TokenGenerator tg = new TokenGenerator();

    // object for generating and saving barcode images
    private BarcodeGenerator bg = new BarcodeGenerator();

    // object for operations on the database
    private DatabaseInterface db = new InMemoryRepository();

    // directory with barcodes images
//    private static String path = "./barcodes_images/";
    private static String path = "barcodes_images/";



    // generate count amount of tokens for a given user
    // create a user if he doesn't exist
    // add new objects to the databases
    public List<TokenRepresentation> generateTokens(String userId, int count, String accountId) {
        System.out.println("userid received: " + userId + ", count received: " + count);
        if (count > 5 || count < 1)
            throw new IllegalArgumentException("The number of requests must be between 1 and 5!");
        List<TokenRepresentation> tokens = new ArrayList<>();
        if(tokensAvailable(userId) < 2){
            System.out.println("We get to generate");
            while (tokens.size() < count) {
                Token t = tg.generateToken(userId, accountId);
                System.out.println("Token: " + t.getTokenValue());
                if(!tokenExists(t.getTokenValue())){
                    System.out.println("Token doesnt exist. Adding it");

                    TokenRepresentation tr = new TokenRepresentation(t.getTokenValue(), bg.generate(t.getTokenValue(), path));
                    tokens.add(tr);
                    db.insertToken(t);
                }
            }
        }
        System.out.println(tokens.size());
        return tokens;
    }


    // use the token with a given string
    public String useToken(String tokenValue) {
        if (!tokenExists(tokenValue))
            throw new NullPointerException("objects.Token not found");
        if (tokenUsed(tokenValue))
            throw new IllegalArgumentException("objects.Token already used!");
        db.updateTokenUsed(tokenValue);
        return db.getUserAccountByToken(tokenValue);
    }

    // does a token with string exist?
    private boolean tokenExists(String token) {
        try{
            Token t = getToken(token);
            return  true;
        }catch(NullPointerException e){
            System.out.println("Returning false, token does not exist");
            return false;
        }
    }

    // is a token with a given string used?
    private boolean tokenUsed(String token) {
        Token tmp = getToken(token);
        return tmp.isUsed();
    }

    // returns a token with given string
    private Token getToken(String token) {
        try {

            Token t = db.getToken(token);
            System.out.println("Trying to get token");
            return t;
        }catch(NullPointerException e){
            System.out.println("TokenManager caught exception");
            throw new NullPointerException();
        }

    }


    // how many not used tokens does a given user have?
    public int tokensAvailable(String userId) {
        return db.getAvailableTokens(userId).size();
    }
}
