package objects;


public class UserRepresentation {
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    int userId;
    String username;

    public UserRepresentation(int userId, String username){
        this.userId = userId;
        this.username = username;
    }

    public UserRepresentation(){
        super();
    }
}
