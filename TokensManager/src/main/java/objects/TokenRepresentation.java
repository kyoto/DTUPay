package objects;


import java.net.URI;
/*
 @Author = Kacper
 */
public class TokenRepresentation {
    public String getTokenValue() {
        return tokenValue;
    }

    public URI getBarcodeURI() {
        return barcodeURI;
    }

    public void setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
    }

    public void setBarcodeURI(URI barcodeURI) {
        this.barcodeURI = barcodeURI;
    }

    private String tokenValue;
    private URI barcodeURI;



    public TokenRepresentation(String tokenValue, URI barcodeURI){
        this.tokenValue = tokenValue;
        this.barcodeURI = barcodeURI;
    }

    public TokenRepresentation(){
        super();
    }
}
