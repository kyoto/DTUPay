package objects;

/*
 @Author = Kacper
 */
public class Token {
    private boolean used;
    private String tokenValue;
    private String userId;
    private String accountId;

    public Token() {
    }

    public Token(String tokenValue, String userId, String accountId) {
        this.used = false;
        this.tokenValue = tokenValue;
        this.userId = userId;
        this.accountId = accountId;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public String getTokenValue() {
        return tokenValue;
    }

    public void setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
