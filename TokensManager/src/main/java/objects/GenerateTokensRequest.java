package objects;

/*
 @Author = Jacob
 */
public class GenerateTokensRequest {
    private String cpr;
    private String accountId;
    private int count;

    public String getCpr() {
        return cpr;
    }

    public void setCpr(String cpr) {
        this.cpr = cpr;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public GenerateTokensRequest() {
    }

    public GenerateTokensRequest(String cpr, String accountId, int count) {
        this.cpr = cpr;
        this.accountId = accountId;
        this.count = count;
    }
}
