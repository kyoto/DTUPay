package generators;

import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeImageHandler;
import net.sourceforge.barbecue.output.OutputException;

import java.io.File;
import java.net.URI;
import java.nio.file.Paths;

import static net.sourceforge.barbecue.BarcodeFactory.createCode128;

/*
 @Author = Kacper
 */
public class BarcodeGenerator {

    // generates and SAVES a barcode of a given key string
    public URI generate(String key, String img_dir) {
        System.out.println("Key: " + key);
        Barcode barcode = null;
        try {
            barcode = createCode128(key);
        } catch (BarcodeException e) {
            e.printStackTrace();
        }
        assert barcode != null;
        barcode.setResolution(50);
        String path = "";
        URI uri;
        System.out.println("Trying to save PNG");
        try {
            path = img_dir + key +".png";
            System.out.println("Running PNG Save function: " + path);
            BarcodeImageHandler.savePNG(barcode, new File(path));
            System.out.println("We saved it at : " + path);
        } catch (OutputException e) {
            System.out.println("Failed to save at : " + img_dir);
            e.printStackTrace();
        }
        uri = Paths.get(path).toUri();

        return uri;
    }
}
