package generators;

import objects.Token;

/*
 @Author = Amal
 */
public class TokenGenerator {
    private String generateRandomString() {
        return RandomStringGenerator.generate();
    }
    public Token generateToken(String userId, String accountId) {
        return new Token(generateRandomString(), userId, accountId);
    }
}
