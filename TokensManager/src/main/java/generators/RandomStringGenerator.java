package generators;

/*
 @Author = Hadi
 */
public class RandomStringGenerator {

    static final String ALPHA_NUMERIC_STRING = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_";


    public static String generate() {
        StringBuilder builder = new StringBuilder();
        int count = 20;
        while (count-- > 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }
}
