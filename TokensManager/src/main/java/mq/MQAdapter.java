package mq;


import com.rabbitmq.client.*;

import gherkin.deps.com.google.gson.Gson;
import manager.TokenManager;
import objects.GenerateTokensRequest;
import objects.Message;
import objects.TokenRepresentation;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.concurrent.TimeoutException;


/*
 @Author = Thomas
 */
public class MQAdapter {

    private static final String EXCHANGENAME = "Tokens";
    private ConnectionFactory factory;
    private TokenManager tokenManager;

    public MQAdapter() {
        factory = new ConnectionFactory();
        factory.setHost("rabbitmq");
//        factory.setHost("localhost");
        tokenManager = new TokenManager();

    }

    public void run() throws IOException, TimeoutException {
        try (Connection connection = factory.newConnection();
            Channel channel = connection.createChannel()) {
            channel.queueDeclare(EXCHANGENAME, false, false, false, null);

            String[] types = {"use", "generate"};

            channel.exchangeDeclare(EXCHANGENAME, "topic");
            String queueName = channel.queueDeclare().getQueue();

            for(String type : types){
                channel.queueBind(queueName, EXCHANGENAME, type);
            }

            System.out.println(" [x] Awaiting objects.Token requests");

            Object monitor = new Object();
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                        .Builder()
                        .correlationId(delivery.getProperties().getCorrelationId())
                        .build();

                String response = "";

                try {
                   response = parseBody(delivery);

                } catch (RuntimeException e) {
                    System.out.println(" [.] " + e.toString());
                } finally {
                    channel.basicPublish("", delivery.getProperties().getReplyTo(), replyProps, response.getBytes("UTF-8"));
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                    // RabbitMq consumer worker thread notifies the RPC server owner thread
                    synchronized (monitor) {
                        monitor.notify();
                    }
                }
            };

            channel.basicConsume(queueName, false, deliverCallback, (consumerTag -> {
            }));
            // Wait and be prepared to consume the message from RPC client.
            while (true) {
                synchronized (monitor) {
                    try {
                        monitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private String parseBody(Delivery delivery) throws UnsupportedEncodingException {

        String message = "";
        String response = "";
        Message responseMessage = new Message();
        Gson gson = new Gson();
        if(delivery.getEnvelope().getRoutingKey().equals("use")){

            message += new String(delivery.getBody(), "UTF-8");
            Message useTokenMessage = gson.fromJson(message, Message.class);
            String tokenValue = useTokenMessage.getPayload();
            System.out.println("Token received: " + tokenValue);
            try {
                response = tokenManager.useToken(tokenValue);
                responseMessage.setStatus("OK");
            }catch(Exception e){
                response = e.getMessage();
                responseMessage.setStatus("ERROR");
            }

        }else if(delivery.getEnvelope().getRoutingKey().equals("generate")){

            message = new String(delivery.getBody(), "UTF-8");
//            JSONObject obj = new JSONObject(message);
            Message generateTokensMessage = gson.fromJson(message, Message.class);

            System.out.println("Object received: " + generateTokensMessage.getPayload());
            GenerateTokensRequest gtr = gson.fromJson(generateTokensMessage.getPayload(), GenerateTokensRequest.class);
            try {
                List<TokenRepresentation> tokens = tokenManager.generateTokens(gtr.getCpr(), gtr.getCount(), gtr.getAccountId());
                response = gson.toJson(tokens);
                responseMessage.setStatus("OK");
            }catch (Exception e){
                response = e.getMessage();
                responseMessage.setStatus("ERROR");
            }
        }
        responseMessage.setPayload(response);
        return gson.toJson(responseMessage);
    }
}