import java.util.Objects;

/*
 @Author = Kacper
 */
public class Merchant {
    private String cvr;
    private String username;

    public String getCvr() {
        return cvr;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setCvr(String cvr) {
        this.cvr = cvr;
    }

    public Merchant() {
    }

    public Merchant(String cvr, String username) {
        this.cvr = cvr;
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Merchant merchant = (Merchant) o;
        return Objects.equals(cvr, merchant.cvr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cvr);
    }
}
