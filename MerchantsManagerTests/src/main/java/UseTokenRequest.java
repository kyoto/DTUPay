
/*
 @Author = Jacob
 */
public class UseTokenRequest {
    private String tokenValue;
    private int price;
    private String CVR;
    private String paymentMessage;

    public String getTokenValue() {
        return tokenValue;
    }

    public void setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCVR() {
        return CVR;
    }

    public void setCVR(String CVR) {
        this.CVR = CVR;
    }


    public String getPaymentMessage() {
        return paymentMessage;
    }

    public void setPaymentMessage(String paymentMessage) {
        this.paymentMessage = paymentMessage;
    }

    public UseTokenRequest() {
    }

    public UseTokenRequest(String tokenValue, int price, String CVR, String paymentMessage) {
        this.tokenValue = tokenValue;
        this.price = price;
        this.CVR = CVR;
        this.paymentMessage = paymentMessage;
    }
}
