import gherkin.deps.com.google.gson.Gson;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/*
 @Author = Kacper
 */
public class RESTClient {
    Client client = ClientBuilder.newClient();
    Gson gson = new Gson();

    public Response createMerchant(String CVR, String username) {

        WebTarget target = client.target("http://localhost:8080").path("/merchants");
        Merchant merchant = new Merchant(CVR, username);
        String json = gson.toJson(merchant);
        Response response = target
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.entity(json, MediaType.APPLICATION_JSON));
        return response;
    }


    public Response deleteMerchant(String CVR){
        WebTarget target = client.target("http://localhost:8080").path("/merchants");
        Response response = target.path(CVR)
                .request(MediaType.TEXT_PLAIN)
                .delete();
        return response;
    }

    public Response useToken(String tokenValue, int price, String CVR, String message){
        WebTarget target = client.target("http://localhost:8080").path("/merchants/payments");
        UseTokenRequest useTokenRequest = new UseTokenRequest(tokenValue, price, CVR, message);
        String json = gson.toJson(useTokenRequest);
        Response response = target
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.entity(json, MediaType.APPLICATION_JSON));
        return response;
    }
}

