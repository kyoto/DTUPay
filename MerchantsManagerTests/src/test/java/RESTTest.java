import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;

/*
 @Author = Amal
 */
public class RESTTest {
    RESTClient client = new RESTClient();


    //Create and delete Merchant
    @Test
    public void manageMerchant(){
        String CVR = "123456";
        String username = "testName";
        Response response = client.createMerchant(CVR, username);
        Assert.assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
        response = client.deleteMerchant(CVR);
        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }


}
