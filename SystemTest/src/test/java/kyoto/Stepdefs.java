package kyoto;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import static org.junit.Assert.*;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import SystemTest.generated.ws.BankService;
import SystemTest.generated.ws.BankServiceException_Exception;
import SystemTest.generated.ws.BankServiceService;
import SystemTest.generated.ws.User;
import gherkin.deps.com.google.gson.Gson;
import gherkin.deps.com.google.gson.reflect.TypeToken;
import objects.Customer;
import objects.Merchant;
import rest.CustomerClient;
import rest.MerchantClient;
import objects.TokenRepresentation;


import javax.ws.rs.core.Response;
/*
 @Author = Jacob
 */
public class Stepdefs {


	private Customer customer;
	private Merchant merchant;

	private String paymentMessage = "TestPayment";

	private BigDecimal balance = new BigDecimal(1000);

	private BankServiceService bankServiceService = new BankServiceService();
	private BankService bankService = bankServiceService.getBankServicePort();


	private CustomerClient customerClient = new CustomerClient();
	private MerchantClient merchantClient = new MerchantClient();

	private ArrayList<TokenRepresentation> tokens = new ArrayList<>();

	@Given("^a registered customer with a bank account$")
	public void a_registered_customer_with_a_bank_account() {
		// Create a bank account for the database with CPR customerCPR and name
		// customerName
		// Send a createCustomer request to create a database with CPR customerCPR and
		// name customerName
		String customerCPR = "0210";
		String customerFirstName = "John";
		String customerLastName = "Johnson";


		User user = new User();
		user.setCprNumber(customerCPR);
		user.setFirstName(customerFirstName);
		user.setLastName(customerLastName);

		String account = null;

		try {
			bankService.getAccount(bankService.getAccountByCprNumber(customerCPR).getId());
			try {
				bankService.retireAccount(bankService.getAccountByCprNumber(customerCPR).getId());
			} catch (BankServiceException_Exception e) {
				e.printStackTrace();
				fail();
			}
		} catch (BankServiceException_Exception e) {
		}

		try {
			account = bankService.createAccountWithBalance(user, balance);
		} catch (BankServiceException_Exception e1) {
			e1.printStackTrace();
			fail();
		}


		customer = new Customer(customerCPR, account, customerFirstName);

		customerClient.deleteCustomer(customerCPR);

		Response response = customerClient.createCustomer(customer);
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

	}

	@Given("^a registered merchant with a bank account$")
	public void a_registered_merchant_with_a_bank_account() throws Exception {
		// Create a bank account for the merchant with CVR merchantCVR and name
		// merchantName
		// Send a createMerchant request to crate merchant with CVR merchantCVR and name
		// merchantName

		String merchantCVR = "1010";
		String merchantFirstName = "Peter";
		String merchantLastName = "Petersen";

		User user = new User();
		user.setCprNumber(merchantCVR);
		user.setFirstName(merchantFirstName);
		user.setLastName(merchantLastName);

		String account = null;

		try {
			bankService.getAccount(bankService.getAccountByCprNumber(merchantCVR).getId());
			try {
				bankService.retireAccount(bankService.getAccountByCprNumber(merchantCVR).getId());
			} catch (BankServiceException_Exception e) {
				e.printStackTrace();
				fail();
			}
		} catch (BankServiceException_Exception e) {
			//e.printStackTrace();
		}

		try {
			account = bankService.createAccountWithBalance(user, balance);
		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
			fail();
		}

		merchantClient.deleteMerchant(merchantCVR);

		merchant = new Merchant(merchantCVR, account, merchantFirstName);

		Response response = merchantClient.createMerchant(merchant);
		assertEquals(Response.Status.CREATED.getStatusCode(),response.getStatus());

	}

	@Given("^the customer has one unused token$")
	public void the_customer_has_one_unused_token() throws Exception {

		// Generate 1 token for the created database
		Response response = customerClient.generateTokens(customer.getCpr(), 1);
		String jsonString = response.readEntity(String.class);
		System.out.println("Generate tokens response: " + jsonString);
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Type listType = new TypeToken<List<TokenRepresentation>>(){}.getType();
		System.out.println(jsonString);
		tokens = new Gson().fromJson(jsonString, listType);

		assertEquals(1, tokens.size());

		//throw new PendingException();
	}

	private String paymentResult;

	@When("^requests payment for (\\d+) kroner using the token$")
	public void requests_payment_for_kroner_using_the_token(int price) throws Exception {
		// Send a request to use a token and make a payment for price from the database account to
		// merchant account
		String tokenValue = tokens.get(0).getTokenValue();

		Response response = merchantClient.useToken(tokenValue, price, merchant.getCvr(), paymentMessage);

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		paymentResult = response.readEntity(String.class);
		//throw new PendingException();
	}

	@Then("^the payment succeeds$")
	public void the_payment_succeeds() throws Exception {
		// Get the payment confirmation

		assertEquals("SUCCESS", paymentResult);
		//throw new PendingException();
	}

	@Then("^the money is transferred from the customer bank account to the merchant bank account$")
	public void the_money_is_transferred_from_the_customer_bank_account_to_the_merchant_bank_account()
			throws Exception {
		// Check if the amount of money equals to price was subtracted from the database
		// account and added to merchant account
		BigDecimal merchantBalance = bankService.getAccount(merchant.getAccountNumber()).getBalance();
		BigDecimal customerBalance = bankService.getAccount(customer.getAccountNumber()).getBalance();

		assertEquals(new BigDecimal(900), customerBalance);
		assertEquals(new BigDecimal(1100), merchantBalance);

        // Deleting database account from the Bank:
        try {
			bankService.retireAccount(customer.getAccountNumber());
		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
			fail();
		}

        // Deleting merchant account from the Bank
        try {
			bankService.retireAccount(merchant.getAccountNumber());
		} catch (BankServiceException_Exception e) {
			e.printStackTrace();
			fail();
		}

		Response deleteCustomer = customerClient.deleteCustomer(customer.getCpr());
		assertEquals(Response.Status.OK.getStatusCode(), deleteCustomer.getStatus());

		Response deleteMerchant = merchantClient.deleteMerchant(merchant.getCvr());
        assertEquals(Response.Status.OK.getStatusCode(), deleteMerchant.getStatus());

		//throw new PendingException();
	}

}