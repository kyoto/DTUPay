package objects;

import java.util.Objects;

/*
 @Author = Kacper
 */
public class Merchant {

    private String cvr;
    private String username;
    private String accountNumber;

    public Merchant(String cvr, String accountNumber, String username) {
        this.cvr = cvr;
        this.username = username;
        this.accountNumber = accountNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Merchant merchant = (Merchant) o;
        return Objects.equals(cvr, merchant.cvr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cvr);
    }

    public Merchant() { }

    public String getCvr() {
        return cvr;
    }

    public String getUsername() {
        return username;
    }

    public String getAccountNumber() { return accountNumber; }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setCvr(String cvr) {
        this.cvr = cvr;
    }

    public void setAccountNumber(String accountNumber) { this.accountNumber = accountNumber; }
}
