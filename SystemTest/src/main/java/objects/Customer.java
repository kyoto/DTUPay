package objects;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

/*
 @Author = Hadi
 */
@JsonPropertyOrder({"username" , "cpr"})
public class Customer {

	private String username;
	private String cpr;
	private String accountNumber;


	public Customer(String cpr, String accountNumber, String username) {
		this.username = username;
		this.cpr = cpr;
		this.accountNumber = accountNumber;
	}
	
	public Customer() {super();}

	public String getUsername() {
		return username;
	}

	public String getCpr() { return cpr; }

	public String getAccountNumber() { return accountNumber; }

	public void setCustomerUsername(String username) {
		this.username = username;
	}
	
	public void setCustomerCPR(String cpr) {
		this.cpr = cpr;
	}


	@Override
	public boolean equals(Object o) {
		System.out.println(((Customer) o).cpr + ", " + ((Customer) o).cpr);
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Customer customer = (Customer) o;

		return cpr.equals(customer.cpr);
	}

	@Override
	public int hashCode() {
		return Objects.hash(cpr);
	}

	public void setAccountNumber(String accountNumber) { this.accountNumber = accountNumber; }
}
