package rest;

import gherkin.deps.com.google.gson.Gson;
import objects.Customer;
import objects.Merchant;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/*
 @Author = Amal
 */
public class CustomerClient {

    Client client = ClientBuilder.newClient();
    Gson gson = new Gson();

    public Response createCustomer(Customer customer){
        WebTarget target = client.target("http://localhost:8082").path("/customers");

        String json = gson.toJson(customer);
        return target
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.entity(json, MediaType.APPLICATION_JSON));
    }

    public Response deleteCustomer(String cpr){
        WebTarget target = client.target("http://localhost:8082").path("/customers");

        return target.path(cpr)
                .request(MediaType.TEXT_PLAIN)
                .delete();
    }
    public Response generateTokens(String cpr, int count){
        WebTarget target = client.target("http://localhost:8082").path("/customers/tokens");

        return target.queryParam("count", count)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(cpr, MediaType.TEXT_PLAIN));
    }
}
