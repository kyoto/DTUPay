package rest;

import gherkin.deps.com.google.gson.Gson;
import objects.Merchant;
import objects.UseTokenRequest;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/*
 @Author = Thomas
 */
public class MerchantClient {
    Client client = ClientBuilder.newClient();
    Gson gson = new Gson();

    public Response createMerchant(Merchant merchant) {

        WebTarget target = client.target("http://localhost:8081").path("/merchants");


        String json = gson.toJson(merchant);
        return target
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.entity(json, MediaType.APPLICATION_JSON));
    }

    public Response deleteMerchant(String cvr){

        WebTarget target = client.target("http://localhost:8081").path("/merchants");
        return target.path(cvr)
                .request(MediaType.TEXT_PLAIN)
                .delete();
    }

    public Response useToken(String tokenValue, int price, String cvr, String message){
        WebTarget target = client.target("http://localhost:8081").path("/merchants/payments");

        UseTokenRequest useTokenRequest = new UseTokenRequest(tokenValue, price, cvr, message);
        String json = gson.toJson(useTokenRequest);
        return target
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.entity(json, MediaType.APPLICATION_JSON));
    }
}
