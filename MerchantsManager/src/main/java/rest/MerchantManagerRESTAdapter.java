package rest;

import objects.Merchant;
import objects.UseTokenRequest;
import manager.*;

import gherkin.deps.com.google.gson.Gson;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/*
 @Author = Kacper
 */
@Path("/merchants")
public class MerchantManagerRESTAdapter {
    private static MerchantManager merchantManager = new MerchantManager();
    private Gson gson = new Gson();

    @GET
    @Produces("text/plain")
    public Response doGet(){
        return Response.ok("Merchants Microservices").build();
    }


    @POST
    @Consumes("application/json")
    public Response createMerchant(String data){
        Merchant merchant = gson.fromJson(data, Merchant.class);
        if(merchantManager.createMerchant(merchant))
            return Response.status(Response.Status.CREATED).build();
        return Response.status(Response.Status.FORBIDDEN)
                .entity("objects.Merchant already exists")
                .build();
    }

    @Path("/{CVR}")
    @DELETE
    public Response deleteMerchant(@PathParam("CVR") String CVR){
        if(merchantManager.deleteMerchant(CVR)) {
            return Response.ok().build();
        }
        return Response.status(Response.Status.NOT_FOUND)
                .entity("objects.Merchant not found")
                .build();
    }


    @Path("/payments")
    @POST
    @Consumes("application/json")
    @Produces("text/plain")
    public Response useToken(String data){
        String result;
        System.out.println("Data received from test: " + data);
        UseTokenRequest useTokenRequest = gson.fromJson(data, UseTokenRequest.class);

        try {
            result = merchantManager.useToken(useTokenRequest);
        }catch (Exception e){
            return Response.status(Response.Status.FORBIDDEN)
                    .entity(e.getMessage())
                    .build();
        }

        return Response.status(Response.Status.OK).entity(result).build();
    }

}
