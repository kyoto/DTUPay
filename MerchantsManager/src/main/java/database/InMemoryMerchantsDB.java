package database;

import objects.Merchant;

import java.util.ArrayList;

/*
 @Author = Kacper
 */
public class InMemoryMerchantsDB implements IMerchantsDatabase {

    private ArrayList<Merchant> merchantsDB = new ArrayList<>();

    @Override
    public void insertMerchant(Merchant merchant) {
        merchantsDB.add(merchant);
    }

    @Override
    public void deleteMerchant(String cvr) {
        for(int i = 0; i<merchantsDB.size(); i++){
            if(merchantsDB.get(i).getCvr().equals(cvr))
                merchantsDB.remove(i);
        }
    }

    @Override
    public boolean containsMerchant(String cvr) {
        for (Merchant m: merchantsDB) {
            if(m.getCvr().equals(cvr))
                return true;
        }
        return false;
    }

    @Override
    public Merchant getMerchant(String cvr) {
        for (Merchant m: merchantsDB) {
            if(m.getCvr().equals(cvr))
                return m;
        }
        return null;
    }
}
