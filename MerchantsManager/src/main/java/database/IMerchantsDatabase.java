package database;

import objects.Merchant;

/*
 @Author = Kacper
 */
public interface IMerchantsDatabase {
    void insertMerchant(Merchant merchant);
    void deleteMerchant(String cvr);
    boolean containsMerchant(String cvr);
    Merchant getMerchant(String cvr);
}
