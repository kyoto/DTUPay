package manager;

import database.IMerchantsDatabase;
import database.InMemoryMerchantsDB;
import mq.BankManagerMQCommunicator;
import mq.IBankManagerCommunicator;
import mq.ITokensManagerCommunicator;
import mq.TokenManagerMQCommunicator;
import objects.Merchant;
import objects.UseTokenRequest;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Response;

/*
 @Author = Kacper
 */
public class MerchantManager {

    private IMerchantsDatabase merchantsDB = new InMemoryMerchantsDB();

    private ITokensManagerCommunicator tokenManagerCommunicator = new TokenManagerMQCommunicator();

    private IBankManagerCommunicator bankManagerCommunicator = new BankManagerMQCommunicator();

    public String useToken(UseTokenRequest useTokenRequest) {
        System.out.println("UseTokenRequest: " + useTokenRequest.getCvr());
        String debtorAccount = "";
        try {
            debtorAccount = tokenManagerCommunicator.useToken(useTokenRequest.getTokenValue());
        }catch (Exception e){
            System.out.println("Failed to use token");
            throw new ForbiddenException(e.getMessage());
        }
        try{
            Merchant m = merchantsDB.getMerchant(useTokenRequest.getCvr());
            return bankManagerCommunicator.requestPayment(m.getAccountNumber(), debtorAccount,
                    useTokenRequest.getPrice(), useTokenRequest.getPaymentMessage());
        }
        catch (Exception e) {
            throw new ForbiddenException(e.getMessage());
        }
    }

    public boolean createMerchant(Merchant merchant){
        if(!merchantsDB.containsMerchant(merchant.getCvr())) {
            merchantsDB.insertMerchant(merchant);
            return true;
        }
        return false;
    }

    public boolean deleteMerchant(String cvr){
        if(merchantsDB.containsMerchant(cvr)) {
            merchantsDB.deleteMerchant(cvr);
            return true;
        }
        return false;
    }
}
