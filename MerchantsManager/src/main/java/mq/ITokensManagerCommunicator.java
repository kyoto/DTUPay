package mq;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/*
 @Author = Gudmundur
 */
public interface ITokensManagerCommunicator {
    String useToken(String tokenValue)throws IOException, InterruptedException, TimeoutException;

}
