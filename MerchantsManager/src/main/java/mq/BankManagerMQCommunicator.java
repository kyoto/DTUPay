package mq;

import objects.Message;
import objects.PaymentRequest;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import gherkin.deps.com.google.gson.Gson;


import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;

/*
 @Author = Jacob
 */
public class BankManagerMQCommunicator implements IBankManagerCommunicator {

    private Connection connection;
    private Channel channel;
    private String QUEUE_NAME = "Tokens";
    private ConnectionFactory factory = new ConnectionFactory();
    private Gson gson = new Gson();



    @Override
    public String requestPayment(String creditorAccount, String debtorAccount, int price, String message) throws IOException, InterruptedException, TimeoutException  {
        factory.setHost("rabbitmq");
//        factory.setHost("localhost");

        connection = factory.newConnection();
        channel = connection.createChannel();

        final String corrId = UUID.randomUUID().toString();

        PaymentRequest paymentRequest = new PaymentRequest(creditorAccount,debtorAccount, price, message);
        String payload = gson.toJson(paymentRequest);
        Message paymentMessage = new Message("OK", payload);
        String json = gson.toJson(paymentMessage);

        String replyQueueName = channel.queueDeclare().getQueue();
        AMQP.BasicProperties props = new AMQP.BasicProperties
                .Builder()
                .correlationId(corrId)
                .replyTo(replyQueueName)
                .build();

        channel.basicPublish("", QUEUE_NAME, props, json.getBytes("UTF-8"));

        final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);

        String ctag = channel.basicConsume(replyQueueName, true, (consumerTag, delivery) -> {
            if (delivery.getProperties().getCorrelationId().equals(corrId)) {
                response.offer(new String(delivery.getBody(), "UTF-8"));
            }
        }, consumerTag -> {
        });

        String result = response.take();
        Message resultMessage = gson.fromJson(result, Message.class);
        if(!resultMessage.getStatus().equals("OK"))
            throw new RuntimeException("Transaction failed");
        channel.basicCancel(ctag);
        connection.close();
        return resultMessage.getPayload();

    }
}
