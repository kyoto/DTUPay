package mq;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/*
 @Author = Amal
 */
public interface IBankManagerCommunicator {
    String requestPayment(String CVR, String CPR, int price, String message)throws IOException, InterruptedException, TimeoutException;
}
