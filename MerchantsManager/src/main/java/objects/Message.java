package objects;

public class Message {
    private String status;
    private String payload;

    public String getStatus() {        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Message(String status, String payload) {
        this.status = status;
        this.payload = payload;
    }

    public Message() {
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }
}
