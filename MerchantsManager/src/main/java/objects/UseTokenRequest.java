package objects;

public class UseTokenRequest {
    private String tokenValue;
    private int price;
    private String cvr;
    private String paymentMessage;

    public String getTokenValue() {
        return tokenValue;
    }

    public void setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCvr() {
        return cvr;
    }

    public void setCvr(String cvr) {
        this.cvr = cvr;
    }

    public String getPaymentMessage() {
        return paymentMessage;
    }

    public void setPaymentMessage(String paymentMessage) {
        this.paymentMessage = paymentMessage;
    }

    public UseTokenRequest() {
    }

    public UseTokenRequest(String tokenValue, int price, String cvr, String paymentMessage) {
        this.tokenValue = tokenValue;
        this.price = price;
        this.cvr = cvr;
        this.paymentMessage = paymentMessage;
    }
}
