package objects;

public class PaymentRequest {
    private String creditorAccountNumber;
    private  String debtorAccountNumber;
    private int price;
    private String paymentMessage;

    public String getCreditorAccountNumber() {
        return creditorAccountNumber;
    }

    public void setCreditorAccountNumber(String creditorAccountNumber) {
        this.creditorAccountNumber = creditorAccountNumber;
    }

    public String getDebtorAccountNumber() {
        return debtorAccountNumber;
    }

    public void setDebtorAccountNumber(String debtorAccountNumber) {
        this.debtorAccountNumber = debtorAccountNumber;
    }

    public int getPrice() {
        return price;
    }

    public PaymentRequest() {
    }

    public PaymentRequest(String creditorAccountNumber, String debtorAccountNumber, int price, String paymentMessage) {
        this.creditorAccountNumber = creditorAccountNumber;
        this.debtorAccountNumber = debtorAccountNumber;
        this.price = price;
        this.paymentMessage = paymentMessage;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getPaymentMessage() {
        return paymentMessage;
    }

    public void setPaymentMessage(String paymentMessage) {
        this.paymentMessage = paymentMessage;
    }
}
