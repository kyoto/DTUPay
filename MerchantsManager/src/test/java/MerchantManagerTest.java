import database.IMerchantsDatabase;
import database.InMemoryMerchantsDB;
import objects.Merchant;
import org.junit.Assert;
import org.junit.Test;

/*
 @Author = Hadi
 */
public class MerchantManagerTest {
    private IMerchantsDatabase database = new InMemoryMerchantsDB();

    @Test
    public void addingMerchant() {
        Merchant merchant = new Merchant("123488888", "123", "Merchanky");
        database.insertMerchant(merchant);
        Assert.assertTrue(database.containsMerchant("123488888"));
    }

    @Test
    public void deletingMerchant() {
        Merchant merchant = new Merchant("112554321", "456", "Merchansky");
        database.insertMerchant(merchant);
        Assert.assertTrue(database.containsMerchant("112554321"));
        database.deleteMerchant("112554321");
        Assert.assertFalse(database.containsMerchant("112554321"));
    }

    @Test
    public void containingMerchant(){
        Merchant merchant = new Merchant("999954321", "789", "Merchanky");
        database.insertMerchant(merchant);
        Assert.assertTrue(database.containsMerchant("999954321"));
    }
}
